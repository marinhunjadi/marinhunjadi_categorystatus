<?php

class MarinHunjadi_CategoryStatus_Model_Observer
{
	public function catalogCategoryPrepareSave($observer)
	{
		$isActive = null;
		$category = $observer->getEvent()->getCategory();
		$start=$category->getData('start_date');
		$end=$category->getData('end_date');
		$startDate = Mage::getModel('core/date')->timestamp(strtotime($start));
		$endDate = Mage::getModel('core/date')->timestamp(strtotime($end));
		$today = Mage::getModel('core/date')->timestamp(time());
		
		if(!is_null($start) && !is_null($end)){
			if($startDate<=$today && $endDate>=$today) $isActive=true; else $isActive=false;
		}
		elseif(!is_null($start)){
			if($startDate<=$today) $isActive=true; else $isActive=false;
		}
		elseif(!is_null($end)){
			if($endDate>=$today) $isActive=true; else $isActive=false;
		}
		
		if(!is_null($isActive)) $category->setIsActive($isActive);
		return $this;
	}
	
	public function checkCategoriesForStatus()
	{
		$collection = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('*');
		foreach($collection as $cat){
			$fdfdf=count($collection);
			$isActive=null;
			$start=$cat->getData('start_date');
			$end=$cat->getData('end_date');
			//if(is_null($start) && is_null($end)) continue;
			$startDate = Mage::getModel('core/date')->timestamp(strtotime($start));
			$endDate = Mage::getModel('core/date')->timestamp(strtotime($end));
            $today = Mage::getModel('core/date')->timestamp(time());
			
			if(!is_null($start) && !is_null($end)){
			if($startDate<=$today && $endDate>=$today) $isActive=true; else $isActive=false;
			}
			elseif(!is_null($start)){
				if($startDate<=$today) $isActive=true; else $isActive=false;
			}
			elseif(!is_null($end)){
				if($endDate>=$today) $isActive=true; else $isActive=false;
			}
			
			if(!is_null($isActive)){
				$cat->setIsActive($isActive);
				$cat->save();
			}
		}                
	}
}

?>