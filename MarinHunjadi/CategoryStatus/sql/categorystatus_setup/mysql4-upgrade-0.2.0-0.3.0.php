<?php
$installer=$this;
$installer->startSetup();

$installer->removeAttribute('catalog_category', 'start_date');
$installer->removeAttribute('catalog_category', 'end_date');

$installer->endSetup();