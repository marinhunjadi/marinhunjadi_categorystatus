<?php
$installer=$this;
$installer->startSetup();

$installer->addAttribute('catalog_category', 'start_date', array(
    'group'         => 'General Information',	
    'input'         => 'date',
    'type'          => 'datetime',
    'label'         => 'Start Date',          
    'backend'       => "eav/entity_attribute_backend_datetime", 
    'visible'       => 1,   
    'required'      => 0,
    'user_defined' => 0,
    'searchable' => 0,
    'filterable' => 0,
    'comparable'    => 0,
    'visible_on_front' => 1,
    'visible_in_advanced_search'  => 0,
    'is_html_allowed_on_front' => 0,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,		
));

$installer->addAttribute('catalog_category', 'end_date', array(
    'group'         => 'General Information',	    
    'input'         => 'date',
    'type'          => 'datetime',
    'label'         => 'End Date',          
    'backend'       => "eav/entity_attribute_backend_datetime", 
    'visible'       => 1,   
    'required'      => 0,
    'user_defined' => 0,
    'searchable' => 0,
    'filterable' => 0,
    'comparable'    => 0,
    'visible_on_front' => 1,
    'visible_in_advanced_search'  => 0,
    'is_html_allowed_on_front' => 0,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,	
));

$is_active_aid = $this->getAttribute('catalog_category', 'is_active', 'attribute_id');
$start_date_aid = $this->getAttribute('catalog_category', 'start_date', 'attribute_id');
$end_date_aid = $this->getAttribute('catalog_category', 'end_date', 'attribute_id');

$table = $this->getTable('eav_entity_attribute');
$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$query = "SELECT sort_order FROM ".$table." WHERE attribute_id=".$is_active_aid." LIMIT 1;";
$sort_order = $readConnection->fetchOne($query);
$installer->run("
UPDATE {$this->getTable('eav_entity_attribute')} SET sort_order={$sort_order} WHERE attribute_id IN ({$start_date_aid},{$end_date_aid});
");

$installer->endSetup();